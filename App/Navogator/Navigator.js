import React, {Component} from 'react';
import { StyleSheet,
   Text,
   View,
   StatusBar,
   TouchableOpacity,
   Button,
   Image,
   ImageBackground
  } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import {
    createSwitchNavigator,
    createAppContainer,
    createDrawerNavigator,
    createBottomTabNavigator,
    createStackNavigator
  } from 'react-navigation';
  import Feed from '../screens/Feed';
  import Profile from '../screens/Profile';
  import SearchA from '../screens/Search';
  import Settingda from '../screens/Setting'
import { Platform } from '@unimodules/core';

  class App extends Component {
    render() {
      return <AppContainer />;
    }
  }
  export default App;

  
class WelcomeScreen extends Component {
    render() {
      return (
        <ImageBackground source={require('../../assets/gradient.jpg')} style={styles.imgback}>
         {/* style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:'#DAE0E2'}}> */}
           <StatusBar backgroundColor="blue" barStyle="dark-content" />
           <View>
             {/* <Image source={require('../../assets/Intro.gif')} style={styles.IntroImage}/> */}
           </View>
          <TouchableOpacity
          style={{justifyContent:'center',alignItems:'center',backgroundColor:'#0A79DF',borderRadius:50, height:50, width:"40%"}}
            // title="Login"
            onPress={() => this.props.navigation.navigate('Dashboard')}>
              <Text style={styles.TextStyle}>Signin</Text>
            </TouchableOpacity>
          <TouchableOpacity
          style={{justifyContent:'center',alignItems:'center',marginTop:10,backgroundColor:'#0A79DF',borderRadius:30, height:50, width:"40%"}}
          // title="Sign Up"
          onPress={() => alert('button pressed')}> 
          <Text style={styles.TextStyle}>Signup</Text>
          </TouchableOpacity>
          </ImageBackground>
      );
    }
  }
  

  const DashboardTabNavigator = createBottomTabNavigator(
    {
      Feed,
      Profile,
      SearchA
    },{
      animationEnabled: true,
      swipeEnabled:true,
      tabBarPosition: "bottom",
      tabBarOptions: {
       
        activeTintColor: "#000",
        inactiveTintColor:"#d1cece",
        showLabel: true,
        showIcon: true
      }
    },
    {
      navigationOptions: ({ navigation }) => {
        const { routeName } = navigation.state.routes[navigation.state.index];
        return {
          headerTitle: routeName
        };
      }
    }
  );
  const DashboardStackNavigator = createStackNavigator(
    {
      DashboardTabNavigator: DashboardTabNavigator
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerLeft: (
            <Icon
              style={{ paddingLeft: 10 }}
              onPress={() => navigation.openDrawer()}
              name="md-menu"
              size={30}
            />
          )
        };
      }
    }
  );
  
  const AppDrawerNavigator = createDrawerNavigator({
    Dashboard: {
      screen: DashboardStackNavigator,
      navigationOptions: {
        drawerLabel: 'Dashboard',
        drawerIcon: ({ tintColor }) =>
        <Icon name="ios-information-circle"
        size={25}
        // style={{ paddingRight: 10 }}
        color={tintColor} />
      }
    },
    Setting: {
      screen: Settingda,
      navigationOptions: {
        drawerLabel: 'Setting',
        drawerIcon: ({ tintColor }) =>
        <Icon name="logo-skype"
        size={25}
        // style={{ paddingRight: 10 }}
        color={tintColor} />
      }
    }
  }, {
    drawerBackgroundColor:"#EAF0F1",
    activeTintColor:'#000',
    inactiveTintColor:'#0A79DF'
  });
  
  const AppSwitchNavigator = createSwitchNavigator({
    Welcome: { screen: WelcomeScreen },
    Dashboard: { screen: AppDrawerNavigator }
  });
  
  const AppContainer = createAppContainer(AppSwitchNavigator);
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    TextStyle :{
      color:'#fff',
      fontFamily:"Roboto",
      fontSize:25,
    },
    imgback:{
      width:'100%',
      height:'100%',
      justifyContent:'center',
      alignItems:'center'
    },
    IntroImage:{
      width:50,
      height:50,
      justifyContent:'center',
      alignItems:'center'
    }
  });