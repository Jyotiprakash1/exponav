
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, StatusBar } from 'react-native';
import {Icon} from 'native-base';

class Search extends Component {
  static navigationOptions = {
    tabBarIcon: ({tintColor}) => (
      <Icon name ='ios-search' style={{color : tintColor}} />
    )
  }
    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <StatusBar backgroundColor="blue" barStyle="dark-content" />
          <Text>Search</Text>
        </View>
      );
    }
  }
  export default Search;