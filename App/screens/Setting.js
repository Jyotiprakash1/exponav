import React,{Component} from 'react';
import {Text, View,} from 'react-native'

class SettingScreen extends Component {    static navigationOptions = {
      tabBarIcon: ({tintColor}) => (
        <Icon name ="setting" style={{color : tintColor}} />
      )
    }
    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>SettingScreen</Text>
        </View>
      );
    }
  }
export default SettingScreen;