
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, StatusBar } from 'react-native';
import {Icon} from 'native-base';

class Feed extends Component {
  static navigationOptions = {
    tabBarIcon: ({tintColor}) => (
      <Icon name ='ios-home' style={{color : tintColor}} />
    )
  }
    render() {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <StatusBar backgroundColor="blue" barStyle="dark-content" />
          <Text>Feed</Text>
        </View>
      );
    }
  }
  export default Feed;