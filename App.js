import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import MainNavigator from './App/Navogator/Navigator'
import { createStackNavigator } from 'react-navigation'
import Mainscreen from './App/screens/Main'

export default class App extends Component{
  render() {
  return (
    <MainNavigator/>
  );
 }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

// import React, { Component } from 'react';
// import { View, Text, StyleSheet, Button, StatusBar } from 'react-native';
// import Icon from '@expo/vector-icons/Ionicons';
/**
 * - AppSwitchNavigator
 *    - WelcomeScreen
 *      - Login Button
 *      - Sign Up Button
 *    - AppDrawerNavigator
 *          - Dashboard - DashboardStackNavigator(needed for header and to change the header based on the                     tab)
 *            - DashboardTabNavigator
 *              - Tab 1 - FeedStack
 *              - Tab 2 - ProfileStack
 *              - Tab 3 - SettingsStack
 *            - Any files you don't want to be a part of the Tab Navigator can go here.
 */

